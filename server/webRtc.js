var clientMap = new Map();
var serverConnection;
var peerConnectionConfig = {
    'iceServers': [
        {'urls': 'stun:stun.services.mozilla.com'},
        {'urls': 'stun:stun.l.google.com:19302'},
    ]
};

function onLoad() {
    serverConnection = new WebSocket('wss://' + window.location.hostname + ':8443/server');
    serverConnection.onmessage = gotMessageFromServer;
    changeStatus('waiting to start');
}
function start(){
    serverConnection.send(JSON.stringify({type:'serverPageSetup'}));
}

function gotMessageFromServer(message) {
    var dto = JSON.parse(message.data);
    console.log('received %s',dto)
    var peerConnection;
    var uuid = dto.uuid;
    if(dto.type == 'serverPageSetup'){
        changeStatus('server page is on');
    }
    else if(clientMap.get(uuid) == null){
        peerConnection = initializePeerConnection();
        clientMap.set(uuid,peerConnection);
        addConnection(uuid)
    }
    else{
        peerConnection = clientMap.get(uuid);
    }
    if(dto.type){
        switch(dto.type){
            case 'offer':
                console.log('received offer');
                onOfferReceived(dto);
                break;
            case 'ice':
                console.log('received ice');
                onIceReceived(dto);
                break;
        }
    }

    function gotIceCandidate(event) {
        if(event.candidate != null) {
            serverConnection.send(JSON.stringify({type:'ice', ice:event.candidate,uuid:uuid,direction:'client'}));
        }

    }
    function onIceReceived(ice){
        peerConnection.addIceCandidate(ice.ice,function () {
            console.log('add ice succedded');
        },function () {
            console.log('add ice failed');
        });
    }
    function onOfferReceived(sdp){
        peerConnection.setRemoteDescription(sdp,function () {
            console.log('set remote description success');
        },function () {
            console.log('set remote description fail');
        });
        peerConnection.createAnswer(function (answer) {
            var ans = new RTCSessionDescription(answer);
            peerConnection.setLocalDescription(ans,function () {
                console.log('set local description success');
                sendAnswer(ans.sdp);
            },function () {
                console.log('set local description fail');
            });
        },function () {
            console.log('set remote description fail');
        });
    }
    function sendAnswer(sdp){
        console.log('answer created');
        serverConnection.send(JSON.stringify({type:'answer', sdp:sdp,uuid:uuid,direction:'client'}));
    }

    function gotRemoteStream(event) {
        console.log('got remote stream');
        remoteVideo = document.getElementById(uuid);
        remoteVideo.src = window.URL.createObjectURL(event.stream);
    }
    function initializePeerConnection(){
        peerConnection = new RTCPeerConnection(peerConnectionConfig);
        peerConnection.onaddstream = gotRemoteStream;
        peerConnection.onicecandidate = gotIceCandidate;
        return peerConnection;
    }
    function addConnection(uuid){
        var video = document.createElement('video');
        video.id = uuid;
        var connectionsContainer = document.getElementById('connectionsContainer');
        var connectionDiv = document.createElement('div');
        connectionDiv.className = 'connectionDiv';
        var connectionTimer = document.createElement('div');
        connectionTimer.id = 'timer'+uuid
        connectionTimer.innerHTML = '0:00:00';
        connectionDiv.appendChild(video);
        connectionDiv.appendChild(connectionTimer);
        connectionsContainer.appendChild(connectionDiv);
        setInterval(changeValue, 1000);
    }
    function changeValue(){
        var connectionTimer = document.getElementById('timer'+uuid);
        var time = connectionTimer.innerHTML;

        connectionTimer.innerHTML = increaseTime(time);
    }
};

function changeStatus(state){
    var status = document.getElementById('status');
    status.innerHTML = state;
}



function increaseTime(time){
    console.log(time);
    var splited = time.split(':');
    var seconds = splited[2];
    var minutes = splited[1];
    var hours = splited[0];
    ++seconds;
    if (seconds == 60){
        seconds = 0;
        ++minutes;
        if(minutes == 60){
            minutes = 0;
            ++hours
        }
    }
    if(seconds <10){
        seconds = '0'+seconds;
    }
    if(minutes <10){
        if(minutes = 0){
            minutes = '00';
        }
        else{
            minutes = '0'+minutes;

        }
    }
    return hours+":"+minutes+":"+seconds;
}

