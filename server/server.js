const HTTPS_PORT = 8443;
const fs = require('fs');
const https = require('https');
var path = require('path');
var express = require('express');
const WebSocket = require('ws');
const WebSocketServer = WebSocket.Server;
var serverSocket;
var clientSocketMap = new Map();
var jade = require('jade');

const serverConfig = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem'),
};

var app = express();
app.set('view engine', 'jade')

app.get('/', function (req, res) {
    res.render('index');
});

app.get('/webRtc.js', function(req, res) {
    res.sendFile(path.join(__dirname,'webRtc.js'));
});

app.get('/client.js', function(req, res) {
    res.sendFile(path.join(__dirname,'/../client/client.js'));
});


app.get('/server', function(req, res) {
    res.render('server');
});

app.get('/style.css', function(req, res) {
    res.sendFile(path.join(__dirname,'../css/style.css'));
});


httpsServer = https.createServer(serverConfig, app).listen(HTTPS_PORT);

// ----------------------------------------------------------------------------------------

// Create a server for handling websocket calls
var wss = new WebSocketServer({server: httpsServer});

wss.on('connection', function(ws){
    ws.on('message',function(message){
        console.log('received: %s', message);
        var dto = JSON.parse(message);
        if (dto.type ==='serverPageSetup'){
            serverSocket = ws;
            console.log('server page websocket set')
            serverSocket.send(message);
        }
        else if(serverSocket){
            if(dto.direction === 'server'){
                if(!clientSocketMap.get[dto.uuid])
                    clientSocketMap.set(dto.uuid,ws)
                serverSocket.send(message);
            }
            else if(dto.direction ==='client')
            {
                var socket = clientSocketMap.get(dto.uuid)
                if(socket != null){
                    socket.send(message)
                }
            }
        }
        else{
            console.log('server page socket doesn\'t exist');
        }
    });
});

wss.broadcast = function(data) {
    this.clients.forEach(function(client) {
        if(client.readyState === WebSocket.OPEN) {
            client.send(data);
        }
    });
};

console.log('Server running. Visit https://localhost:' + HTTPS_PORT + ' in Firefox/Chrome (note the HTTPS; there is no HTTP -> HTTPS redirect!)');
