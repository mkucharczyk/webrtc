var localVideo;
var remoteVideo;
var peerConnection;
var uuid;

var peerConnectionConfig = {
    'iceServers': [
        {'urls': 'stun:stun.services.mozilla.com'},
        {'urls': 'stun:stun.l.google.com:19302'},
    ]
};

function onLoad() {
    uuid = uuid();
    localVideo = document.getElementById('localVideo');

    serverConnection = new WebSocket('wss://' + window.location.hostname + ':8443');
    serverConnection.onmessage = gotMessageFromServer;

    var constraints = {
        video: true,
        audio: true,
    };

    if(navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).then(getUserMediaSuccess).catch(errorHandler);
    } else {
        alert('Your browser does not support getUserMedia API');
    }

}

function getUserMediaSuccess(stream) {
    localStream = stream;
    localVideo.srcObject = stream;
    changeStatus('waiting to start');
}

function start() {
    peerConnection = new RTCPeerConnection(peerConnectionConfig);
    peerConnection.onicecandidate = gotIceCandidate;
    peerConnection.addStream(localStream);
    peerConnection.createOffer().then(createdDescription).catch(errorHandler);
}

function gotMessageFromServer(message) {
    if(!peerConnection) start();
    console.log(message);
    var signal = JSON.parse(message.data);

    if(signal.sdp) {
        peerConnection.setRemoteDescription(new RTCSessionDescription(signal)).then(function() {
           console.log('remote description set');
           changeStatus('connection set');
        }).catch(errorHandler);
    } else if(signal.ice) {
        console.log('received ice');
        peerConnection.addIceCandidate(signal.ice,function () {
            console.log('add ice succedded');
        },function () {
            console.log('add ice failed');
        });
    }
}

function gotIceCandidate(event) {
    console.log('ice candidate')
    if(event.candidate != null) {
        serverConnection.send(JSON.stringify({type:'ice', ice:event.candidate, 'uuid': uuid,direction:'server'}));
    }
}

function createdDescription(description) {
    console.log('got description');

    peerConnection.setLocalDescription(description).then(function() {
        serverConnection.send(JSON.stringify({type:'offer', sdp:peerConnection.localDescription.sdp, 'uuid': uuid,direction:'server'}));
    }).catch(errorHandler);
}

function errorHandler(error) {
    console.log(error);
}

function uuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
function changeStatus(state){
    var status = document.getElementById('status');
    status.innerHTML = state;
}
